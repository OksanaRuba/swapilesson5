import React, { createContext, useState, useEffect } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';
import Service from '../../services/service';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";


import './app.css';

const NameContext = createContext();
const App = () => {
  //дані користувача(об'єкта)
  const [profileInformation, setProfileInformation] = useState(false);
  useEffect(()=> {
    console.log('upDate')
  })
  //отримання даних користувача(об'єкта)
  const onElementInfo = (value) => {
    setProfileInformation(value);
    //console.dir(value);
  } 
  console.log(profileInformation);
  setProfileInformation(1)
  return (
    <Router>
      <NameContext.Provider value={{ onElementInfo: onElementInfo, profileInformation: profileInformation }}>
      <Header />
      <RandomPlanet />
      <div className="row mb2"> 
        <Route exact path='/people'>
          <div className="col-md-6">
            <ItemList request={new Service().getPeoples()} />
          </div>
        </Route>
        <Route exact path='/planets'>
          <div className="col-md-6">
            <ItemList request={new Service().getPlanets()} />
          </div>
        </Route>
        <Route exact path='/starships'>
          <div className="col-md-6">
            <ItemList request={new Service().getStarships()} />
          </div>
        </Route>
        <div className="col-md-6">
            <PersonDetails /> {/*відображення даних об'єкта*/}
        </div>
      </div>
      </NameContext.Provider>
    </Router>
  );
};

export { NameContext }; //не по дофолту
export default App;